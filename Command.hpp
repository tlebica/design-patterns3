#pragma once
#include "Calculator.hpp"
#include <memory>

using Operation = void (Calculator::*)(int);

class Command
{
public:
    Command(Calculator& calculator, Operation operation, int argument)
        :calculator(calculator),
          operation(operation),
          argument(argument)
    {}

    void doIt()
    {
        state = calculator.getState();
        (calculator.*operation)(argument);
    }

    void undoIt()
    {
        calculator.setState(std::move(state));
    }

    Calculator& calculator;
    Operation operation;
    std::unique_ptr<Memento> state;
    int argument;
};

using CommandPtr = std::shared_ptr<Command>;

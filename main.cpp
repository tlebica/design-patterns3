#include <iostream>
#include <string>
#include <memory>
#include "Calculator.hpp"
#include "Command.hpp"
#include "Executor.hpp"
#include <stack>

Calculator calc;
Executor executor;

void handleOperation(char operation, int argument)
{
    CommandPtr command{};
    switch (operation)
    {
    case '+':
        command = std::make_shared<Command>(calc, &Calculator::add, argument);
        break;
    case '-':
        command = std::make_shared<Command>(calc, &Calculator::subtract, argument);
        break;
    case '*':
        command = std::make_shared<Command>(calc, &Calculator::mulitiply, argument);
        break;
    case '/':
        command = std::make_shared<Command>(calc, &Calculator::divide, argument);
        break;
    case 'u':
        executor.undoIt();
        break;
    case 'r':
        executor.redoIt();
        break;
    default:
        break;
    }
    executor.doIt(command);
}

int main()
{
    std::string line;
    while(true)
    {
        std::getline(std::cin,line);
        if (line == "exit")
        {
            return 0;
        }
        if (line.size() > 0)
        {
            char operation = line[0];
            line = line.substr(1,line.size()-1);
            int argument = std::atoi(line.data());
            handleOperation(operation, argument);
        }
    }
    return 0;
}

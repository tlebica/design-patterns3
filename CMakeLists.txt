project(calculator)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

cmake_minimum_required(VERSION 2.8)

aux_source_directory(. SRC_LIST)
add_definitions("-std=c++14 -Ofast -Wreturn-type -Werror=return-type")
add_executable(${PROJECT_NAME} ${SRC_LIST})
target_link_libraries(${PROJECT_NAME} pthread)


#pragma once

#include <iostream>
#include <memory>

struct Calculator;
struct CalculatorMemento;

struct Visitor
{
    virtual void setState(CalculatorMemento* calcMemento) = 0;
    virtual ~Visitor(){}
};

struct Memento
{
    virtual void accept(Visitor& v) = 0;
    virtual ~Memento(){}
};

class CalculatorMemento : public Memento
{
public:
    CalculatorMemento(int state)
        :state(state)
    {}
    void accept(Visitor& v) override;
    int getState()
    {
        return state;
    }
private:
    int state;
};

class Calculator: public Visitor
{
public:
    void add(int a)
    {
        result += a;
        showResult();
    }
    void subtract(int a)
    {
        result -= a;
        showResult();
    }
    void mulitiply(int a)
    {
        result *= a;
        showResult();
    }
    void divide(int a)
    {
        result /= a;
        showResult();
    }
    void showResult()
    {
        std::cout << "Result " << result << std::endl;
    }
    std::unique_ptr<Memento> getState()
    {
        return std::unique_ptr<Memento>(new CalculatorMemento(result));
    }
    void setState(std::unique_ptr<Memento> memento)
    {
        memento->accept(*this);
    }

    void setState(CalculatorMemento* calcMemento) override
    {
        result = calcMemento->getState();
        showResult();
    }
private:
    int result = 0;
};

#pragma once
#include "Command.hpp"
#include <stack>

class Executor
{
public:
    void doIt(CommandPtr command)
    {
        if (command)
        {
            command->doIt();
            undoStack.push(command);
            std::stack<CommandPtr> empty{};
            redoStack.swap(empty);
        }
    }
    void undoIt()
    {
        if (undoStack.empty())
        {
            std::cout << "No more commands to undo" << std::endl;
        }
        else
        {
            undoStack.top()->undoIt();
            redoStack.push(undoStack.top());
            undoStack.pop();
        }
    }
    void redoIt()
    {
        if (redoStack.empty())
        {
            std::cout << "No more commands to redo" << std::endl;
        }
        else
        {
            redoStack.top()->doIt();
            undoStack.push(redoStack.top());
            redoStack.pop();
        }
    }
private:
    std::stack<CommandPtr> undoStack;
    std::stack<CommandPtr> redoStack;
};
